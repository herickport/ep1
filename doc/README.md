# EP1 - OO 2019.2 (UnB - Gama)

Hérick Ferreira de Souza Portugues - 180033034

## Observações

- Este programa é compatível com o terminal do Linux, por ter sido utilizadas funções especifícas do linux, como o system("clear"), este programa não é compatível com o Windows;
- Com exceção do logomarca da vendinha, em asciiart, não há arquivos salvos no repositório do projeto;
- Evite o uso de acentos, cedilha e caracteres especiais no nome dos produtos e categorias, pois pode prejudicar a visualização dos menus em unicode, visto que a funções size() e length() não retornam o valor correto com o uso desses caracteres;
- O código conta com comentários nos arquivos .hpp explicando cada um dos métodos e alguns comentários no arquivos .cpp.

## Rodando o Programa

Para compilar e rodar o programa, devem ser utilizados os seguintes comandos no terminal:  
   - make clean  
   - make  
   - make run

## Descrição

Ao iniciar o programa é apresentado um menu de utilização com a logomarca da vendinha(Vic's Market) e as opções para que a funcionária possa escolher um dos três modos de operação do sistema, sendo eles:  
   - **Modo Venda**  
   - **Modo Estoque**  
   - **Modo Recomendação**

### Modo Venda
Ao entrar no modo venda é apresentado um menu em que se pode cadastrar um novo cliente ou inserir os dados de um cliente existente e prosseguir para a venda.

Ao iniciar a venda, é apresentado todos os produtos cadastrados no sistema para que seja feita a escolha dos produtos, escolha esta que será feita pelo ID do produto.

Para tornar a vendinha mais dinâmica e prática, ao invés de cancelar toda a compra sempre que ela apresentar algum produto em quantidade maior que a existente em estoque, foi escolhido apresentar, logo após escolher a quantidade, uma mensagem de erro alertando sobre a falta do produto em estoque, possibilitando que o cliente escolha uma quantidade menor e continue com a compra normalmente.

Após finalizar a compra é apresentado o carrinho final com todos os produtos comprados, a quantidade e o preço total. Após mostrar todos os produtos, é apresentado o valor total da compra, o desconto - caso o cliente seja sócio - e o valor total com desconto.

- **Sócio**:  
	Existem duas formas do cliente se tornar sócio: 
    - Ao se cadastrar, o cliente pode se tornar sócio pagando uma taxa de R$5.00 na primeira compra e já passa a receber os 15% de desconto.  
    - Caso o cliente não deseje pagar os R$5.00 para se tornar sócio ao cadastrar, após comprar R$100.00 na loja o cliente se torna sócio.  

### Modo Estoque
Ao entrar no modo estoque é apresentado um menu em que se pode cadastrar um novo produto, visualizar todos os produtos cadastrados e atualizar a quantidade de um determinado produto no estoque. 

- Ao cadastrar um novo produto, é mostrado todas as categorias cadastradas, onde é possível adicionar diversas categorias existentes ao produto ou cadastrar novas categorias. 

### Modo Recomendação
Para listar os itens recomendados para cada cliente, foi feito um algoritmo que leva em conta a quantidade de vezes que o cliente comprou aquele produto e a quantidade de vezes que ele comprou produtos das mesmas categorias daquele determinado produto.

Foi utilizado peso 5 para a quantidade de vezes que ele comporu aquele produto e peso 3 para a quantidade de vezes que foram comprados produtos da mesma categoria do produto analisado. Assim, se determinado cliente já comprou aquele produto 2 vezes e comprou produtos da mesma categoria 4 vezes, o nível de recomendação será: 5x2 + 3x4 = 22. 
 
- Na lista de recomendação é apresentado no máximo 10 produtos, ordenados de acordo com o algoritmo de recomendação. Caso a recomendação seja igual para diferentes produtos, é utilizada a ordem lexicográfica para ordenar.


### Arquivos
Os arquivos utilizados no programa estão salvos em pastas dentro da pasta doc do projeto.

- A pasta categorias armazena um arquivo para cada categoria contendo, em cada arquivo, todos os produtos que fazem parte daquela categoria;
- A pasta clientes armazena um arquivo para cada cliente contendo todas as informações do cliente;
- A pasta estoque armazena um arquivo para cada produto, contendo a quantidade do produto existente no estoque;
- A pasta geral contém três arquivos - categorias.txt(armazena todas as categorias cadastradas), produtos.txt(armazena todos os produtos cadastrados) e clientes.txt(armazena todos os clientes cadastrados);
- A pasta produtos armazena um arquivo para cada produto, contendo todas as informações do produto;
- A pasta vendas armazena um arquivo para cada cliente, contendo todas os produtos comprados pelo cliente, junto com a quantidade.
