#include "categoria.hpp"

#include <iostream>
#include <fstream>

Categoria::Categoria() {}

Categoria::~Categoria() {}

void Categoria::adiciona_categoria(string nome_categoria) {
	ofstream categorias;

	categorias.open("./doc/geral/categorias.txt", ios::app);

	if(categorias.is_open())
		categorias << nome_categoria << endl;
		
	categorias.close(); 
}

vector<string> Categoria::busca_categorias() {
	ifstream arquivo_categorias;
	vector<string> categorias;
	string categoria;

	arquivo_categorias.open("./doc/geral/categorias.txt");

	if(arquivo_categorias.is_open())
		while(getline(arquivo_categorias, categoria))
			categorias.push_back(categoria);

	arquivo_categorias.close();

	return categorias;
}

void Categoria::adiciona_produto(Produto * produto) {
	ofstream categoria;

	for(string cat : produto->get_categorias()) {
		categoria.open("./doc/categorias/" + cat + ".txt", ios::app);

		if(categoria.is_open())
			categoria << produto->get_nome() << endl;

		categoria.close();
	}
}
