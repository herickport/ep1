#include "estoque.hpp"

#include <iostream>
#include <fstream>

using namespace std;

Estoque::Estoque() {}

Estoque::~Estoque() {}

void Estoque::adiciona_estoque(Produto * produto) {
	ofstream estoque;

	estoque.open("./doc/estoque/" + produto->get_nome() + ".txt", ios::app);

	if(estoque.is_open())
		estoque << produto->get_quantidade();

	estoque.close();
}

vector<Produto> Estoque::recupera_estoque() {
	vector<Produto> produtos_estoque;
	vector<string> categorias;
	Produto produtoAuxiliar;
	ifstream estoque;
	ifstream todosProdutos;
	ifstream produto;
	string nome_produto;
	string texto;

	todosProdutos.open("./doc/geral/produtos.txt");

	if(todosProdutos.is_open()) {
		while(getline(todosProdutos, nome_produto)) {
			estoque.open("./doc/estoque/" + nome_produto + ".txt");
			produto.open("./doc/produtos/" + nome_produto + ".txt");
			categorias.erase(categorias.begin(), categorias.end());

			if(estoque.is_open() and produto.is_open()) {
				int linha = 1;
				while(getline(produto, texto)) {
					if(linha == 1)
						produtoAuxiliar.set_ID(stoi(texto));
					if(linha == 2)
						produtoAuxiliar.set_preco(stod(texto));
					if(linha >= 3)
						categorias.push_back(texto);
					linha++;
				}
				getline(estoque, texto);
				
				produtoAuxiliar.set_quantidade(stoi(texto));
				produtoAuxiliar.set_categorias(categorias);
				produtoAuxiliar.set_nome(nome_produto);

				produtos_estoque.push_back(produtoAuxiliar);
			}

			produto.close();
			estoque.close();
		}
	}

	todosProdutos.close();

	return produtos_estoque;
}

void Estoque::atualiza_estoque(Produto produto, int novaQuantidade) {
	ofstream estoqueProduto;

	estoqueProduto.open("./doc/estoque/" + produto.get_nome() + ".txt");

	if(estoqueProduto.is_open())
		estoqueProduto << novaQuantidade;

	estoqueProduto.close();
}

bool Estoque::produto_em_estoque() {
	vector<Produto> produtos;

	produtos = recupera_estoque();
	
	if(produtos.size() != 0)
		return true;
	else
		return false;
}
