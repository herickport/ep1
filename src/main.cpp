#include "cliente.hpp"
#include "produto.hpp"
#include "carrinho.hpp"
#include "estoque.hpp"
#include "interfacemenu.hpp"
#include "interfaceprodutos.hpp"
#include "entrada.hpp"

#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

int main() {

    int comandoPrincipal;
    char aux;

    Produto * produto;
    Carrinho * carrinho;
    Cliente * cliente;
    Estoque * estoque;
    vector<Produto> produtos;

    produto = new Produto();
    carrinho = new Carrinho();
    estoque = new Estoque();

    do {
        system("clear");

        InterfaceMenu menuPrincipal(75);
        InterfaceMenu modoVenda(75);
        InterfaceMenu modoEstoque(75);
        InterfaceMenu modoRecomendacao(75);

        menuPrincipal.gera_menu("Vic's Market", "===================== Menu Principal =====================",
            {"(1) Modo Venda", "(2) Modo Estoque", "(3) Modo Recomendacao", "(0) Sair"});
        cout << "     Comando: ";
        comandoPrincipal = get_input<int>();

        system("clear");
        switch(comandoPrincipal) {
            // Modo Venda
            case 1: {
                int comandoVenda;
                do {
                    system("clear");
                    modoVenda.gera_menu("Vic's Market", "======================= Modo Venda =======================",
                        {"(1) Inserir dados do Cliente e prosseguir", "(2) Cadastrar Cliente", "(0) Retornar ao Menu Principal"});
                    cout << "     Comando: ";
                    comandoVenda = get_input<int>();

                    system("clear");
                    switch(comandoVenda) {
                        case 1:
                            cliente = cliente->valida_cliente();

                            if(cliente->is_socio())
                                cout << "\n  Cliente sócio, prosseguindo para a venda...\n";
                            else
                                cout << "\n  Cliente não sócio, prosseguindo para a venda...\n";
                            sleep(2);

                            system("clear");
                            if(estoque->produto_em_estoque()) {
                                carrinho->lista_produtos();
                                carrinho->executar_compras(cliente);
                            } else {
                                cout << "\n  Não foi possível iniciar a venda, pois não há produtos em estoque...\n";
                                sleep(3);
                            }
                            break;
                        case 2:
                            cliente = cliente->cadastra_cliente();
                            break;
                        case 0:
                            cout << "\n  Retornando ao Menu Principal...\n";
                            sleep(1);
                            break;
                        default:
                            cout << "\n  Entrada inválida! Insira novamente...\n";
                            sleep(1);
                            break;
                    }
                } while(comandoVenda != 0);
            }
                break;
            // Modo Estoque
            case 2: {
                int comandoEstoque;
                do {
                    system("clear");
                    modoEstoque.gera_menu("Vic's Market", "====================== Modo Estoque ======================",
                        {"(1) Cadastrar Produtos", "(2) Visualizar Produtos",
                        "(3) Atualizar Estoque", "(0) Retornar ao Menu Principal"});
                    cout << "     Comando: ";
                    comandoEstoque = get_input<int>();

                    system("clear");
                    switch(comandoEstoque) {
                        case 1:
                            produto->cadastra_produto();
                            break;
                        case 2:
                            if(estoque->produto_em_estoque()) {
                                produto->imprime_produto();
                                cout << "     Presione enter para retornar ao Modo Estoque...";
                                setbuf(stdin, NULL);
                                scanf("%c", &aux);
                            } else {
                                cout << "\n  Ainda não há produtos em estoque, cadastre algum produto primeiro...\n";
                                sleep(2);
                            }
                            break;
                        case 3: {
                            InterfaceProdutos produtosEstoque(65);
                            produtos = estoque->recupera_estoque();

                            if(estoque->produto_em_estoque()) {
                                cout << "\tAtualizar Estoque: \n";
                                produtosEstoque.produtos_estoque(produtos);
                                produto->atualiza_produto(produtos);
                            } else {
                                cout << "\n  Ainda não há produtos em estoque, cadastre algum produto primeiro...\n";
                                sleep(2);
                            }
                        }
                            break;
                        case 0: 
                            cout << "\n     Retornando ao Menu Principal...\n";
                            sleep(1);
                            break;
                        default:
                            cout << "\n     Entrada inválida! Insira novamente...\n";
                            sleep(1);
                            break;
                    }
                } while(comandoEstoque != 0);
            }
                break;
            // Modo Recomendação
            case 3: {
                int comandoRecomendacao;
                do {
                    system("clear");
                    modoRecomendacao.gera_menu("Vic's Market", "=================== Modo Recomendacao ===================",
                        {"(1) Inserir dados do Cliente", "(0) Retornar ao Menu Principal"});
                    cout << "     Comando: ";
                    comandoRecomendacao = get_input<int>();

                    system("clear");
                    switch(comandoRecomendacao) {
                        case 1: {
                            string cpf_cliente;

                            cout << "\n  Digite o cpf do cliente: ";
                            cin >> cpf_cliente;

                            if(cliente->cliente_cadastrado(cpf_cliente)) {
                                system("clear");
                                cliente = cliente->recupera_cliente(cpf_cliente);
                                if(cliente->get_total_compras() > 0) {
                                    cliente->recomenda_produtos(cliente);
                                    cout << "     Presione enter para retornar ao Modo Recomendação...";
                                    setbuf(stdin, NULL);
                                    scanf("%c", &aux);
                                } else {
                                    cout << "\n   O cliente ainda não fez compras...\n";
                                    sleep(2);
                                }
                            } else {
                                cout << "\n   O cliente não está cadastrado, cadastre-o primeiro...\n";
                                sleep(2);
                            }
                        }
                            break;
                        case 0: {
                            cout << "\n  Retornando ao Menu Principal...\n";
                            sleep(1);
                        }
                            break;
                        default: {
                            cout << "\n  Entrada inválida! Insira novamente...\n";
                            sleep(1);
                        }
                            break;
                    }
                } while(comandoRecomendacao != 0);
            }
                break;
            // Sair do Programa
            case 0:
                cout << "\n     Encerrando o Programa...\n";
                sleep(1);
                system("clear");
                break;
            // Entrada Inválida
            default:
                cout << "\n     Entrada inválida! Insira novamente...\n";
                sleep(1);
                break;
        }

    } while(comandoPrincipal != 0);

    return 0;
}
