#include "cliente.hpp"
#include "estoque.hpp"
#include "interfaceprodutos.hpp"
#include "entrada.hpp"

#include <iostream>
#include <fstream>
#include <unistd.h>
#include <algorithm>

Cliente::Cliente() {
	set_nome("");
	set_cpf("");
	set_total_compras(0);
	set_socio(false);
}
	
Cliente::Cliente(string nome, string cpf, bool socio) {
	set_nome(nome);
	set_cpf(cpf);
	set_total_compras(0);
	set_socio(socio);
}

Cliente::Cliente(string nome, string cpf, bool socio, float total_compras) {
	set_nome(nome);
	set_cpf(cpf);
	set_total_compras(total_compras);
	set_socio(socio);
}

Cliente::~Cliente() {}

string Cliente::get_nome() {
	return nome;
}

void Cliente::set_nome(string nome) {
	this->nome = nome;
}

string Cliente::get_cpf() {
	return cpf;
}

void Cliente::set_cpf(string cpf) {
	this->cpf = cpf;
}

float Cliente::get_total_compras() {
	return total_compras;
}

void Cliente::set_total_compras(float total_compras) {
	this->total_compras = total_compras;
}

bool Cliente::is_socio() {
	if(socio)
		return true;
	else
		return false;
}

void Cliente::set_socio(bool socio) {
	this->socio = socio;
}

void Cliente::novo_cliente(Cliente * cliente) {
	ofstream clientes;

	clientes.open("./doc/geral/clientes.txt", ios::app);

	if(clientes.is_open())
		clientes << cliente->get_cpf() << endl;

	clientes.close();
}

Cliente * Cliente::cadastra_cliente() {
	Cliente * cliente;
	string nome_cliente;
	string cpf_cliente;
	int socio;
	char retorno;

	cliente = new Cliente();

	// Mini-Interface para cadastro de cliente
	cout << "\n  \u2554\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550";
	cout << "\u2550\u2550\u2550\u2550\u2550\u2550\u2550 Cadastro de Cliente \u2550\u2550\u2550\u2550\u2550\u2550\u2550";
	cout << "\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\n  \u2551\n";

	cout << "  \u2551  Digite o cpf do cliente: ";
	getline(cin, cpf_cliente);
	
	if(cliente_cadastrado(cpf_cliente)) {
    	cout << "  \u2551\n  \u2551  O cliente já está cadastrado, ";
    	cout << "pressione enter para retornar ao Modo Venda...";
        setbuf(stdin, NULL);
        scanf("%c", &retorno);
    	return cliente;
	}

	cout << "  \u2551  Digite o nome do cliente: ";
	getline(cin, nome_cliente);
	cout << "  \u2551\n  \u2551  Clientes sócios pagam R$5.00 na primeira compra e ganham\n";
	cout << "  \u2551  15% de desconto em todas as compras. Além disso, com R$100.00\n";
	cout << "  \u2551  em compras o cliente se torna sócio da loja.\n";
	cout << "  \u2551\n  \u2551  Deseja ser sócio? (1) Sim / (0) Não - ";

	do {
		socio = get_input<int>();

		if(socio != 0 and socio != 1)
			cout << "  \u2551  Opção inválida, digite novamente: ";
	} while(socio != 0 and socio != 1);

	cliente = new Cliente(nome_cliente, cpf_cliente, socio);

	novo_cliente(cliente);
	arquiva_cliente(cliente);

	system("clear");
	cout << "  Cliente cadastrado com sucesso...\n";
	sleep(1);

	return cliente;
}

Cliente * Cliente::recupera_cliente(string cpf_cliente) {
	Cliente * cliente;
	ifstream arquivo_clientes;

	string texto;
	bool socio;
	string nome;
	float total_compras;

	arquivo_clientes.open("./doc/clientes/" + cpf_cliente + ".txt");

	if(arquivo_clientes.is_open()) {
		int linha = 1;
		while(getline(arquivo_clientes, texto)) {
			if(linha == 1)
				socio = stoi(texto);
			if(linha == 2)
				nome = texto;
			if(linha == 3)
				total_compras = stof(texto);
			linha++;
		}
	}

	cliente = new Cliente(nome, cpf_cliente, socio, total_compras);

	arquivo_clientes.close();

	return cliente;
}

bool Cliente::cliente_cadastrado(string cpf_cliente) {
	ifstream clientes;
	string cpf_arquivo;

	clientes.open("./doc/geral/clientes.txt");

	if(clientes.is_open())
		while(getline(clientes, cpf_arquivo))
			if(cpf_arquivo == cpf_cliente) {
				clientes.close();
				return true;
			}

	clientes.close();

	return false;
}

Cliente * Cliente::valida_cliente() {
	string cpf_cliente;
	Cliente * cliente;

	cout << "\n  Digite o cpf do cliente: ";
	cin >> cpf_cliente;
	setbuf(stdin, NULL);

	if(cliente_cadastrado(cpf_cliente)) {
		cliente = recupera_cliente(cpf_cliente);
	}
	else {
		cout << "\n  Cliente ainda não cadastrado...Faça o cadastro primeiro\n";
		sleep(1);
		cliente = cadastra_cliente();
	}

	return cliente;
}

void Cliente::arquiva_cliente(Cliente * cliente) {
	ofstream arquivo_cliente;

	arquivo_cliente.open("./doc/clientes/" + cliente->get_cpf() + ".txt");

	if(arquivo_cliente.is_open()) {
		arquivo_cliente << cliente->is_socio() << endl;
		arquivo_cliente << cliente->get_nome() << endl;
		arquivo_cliente << cliente->get_total_compras() << endl;
	}

	arquivo_cliente.close();
}

vector<pair<Produto, int>> Cliente::produtos_comprados(Cliente * cliente) {
	Estoque * estoque;
	Produto * produto;
	ifstream arquivo_vendas;
	vector<Produto> produtos;

	int qtd_produtos;
	int quantidade_comprada;
	string texto;
	int posicao = 0;

	produto = new Produto();
	estoque = new Estoque();

	qtd_produtos = produto->quantidade_produtos();

	vector<pair<Produto, int>> produtosComprados(qtd_produtos);
	produtos = estoque->recupera_estoque();

	for(int i = 0; i < qtd_produtos; i++) {
		produtosComprados[i].first.set_ID(produtos[i].get_ID());
		produtosComprados[i].first.set_nome(produtos[i].get_nome());
		produtosComprados[i].first.set_quantidade(produtos[i].get_quantidade());
		produtosComprados[i].first.set_categorias(produtos[i].get_categorias());
		produtosComprados[i].second = 0;
	}

	arquivo_vendas.open("./doc/vendas/" + cliente->get_cpf() + ".txt");

	// Recupera do arquivo a quantidade de cada produto que foi comprada
	if(arquivo_vendas.is_open()) {
		int cont = 1;
		while(getline(arquivo_vendas, texto)) {
			if(cont == 1) {
				quantidade_comprada = stoi(texto);
				cont++;
			} else if(cont == 2) {
				posicao = 0;
				for(pair<Produto, int> prod : produtosComprados) {
					if(texto == prod.first.get_nome())
						break;
					posicao++;
				}
				produtosComprados[posicao].second += quantidade_comprada;
				cont--;
			}
		}
	}

	arquivo_vendas.close();

	return produtosComprados;
} 

vector<pair<string, int>> Cliente::categorias_compradas(vector<pair<Produto, int>> produtosComprados) {
	ifstream arquivo_categorias;
	ifstream produtos_categoria;

	vector<pair<string, int>> categoriasCompradas;

	string categoria;
	string produto;
	int posicao = 0;

	arquivo_categorias.open("./doc/geral/categorias.txt");

	// Recupera do arquivo a quantidade de cada categoria que foi comprada
	if(arquivo_categorias.is_open()) {
		while(getline(arquivo_categorias, categoria)) {
			categoriasCompradas.push_back(make_pair(categoria, 0));
			produtos_categoria.open("./doc/categorias/" + categoria + ".txt");

			if(produtos_categoria.is_open())
				while(getline(produtos_categoria, produto))
					for(pair<Produto, int> prod : produtosComprados)
						if(prod.first.get_nome() == produto) 
							categoriasCompradas[posicao].second += prod.second;

			produtos_categoria.close();
		
			posicao++;
		}
	}

	arquivo_categorias.close();
	
	return categoriasCompradas;
}

// Ordena os produtos recomendados - primeiro de acordo com o peso e depois de acordo com a ordem lexicográfica
bool sortRecomendacao(pair<Produto, int> &a, pair<Produto, int> &b) { 
    if(a.second != b.second) 
    	return (a.second > b.second);
    else
    	return (a.first.get_nome() < b.first.get_nome()); 
} 

void Cliente::recomenda_produtos(Cliente * cliente) {
	vector<pair<Produto, int>> produtosComprados;
	vector<pair<string, int>> categoriasCompradas;
	InterfaceProdutos recomendados(65);

	produtosComprados = produtos_comprados(cliente);
	categoriasCompradas = categorias_compradas(produtosComprados);

	vector<pair<Produto, int>> pesoRecomendacao;

	for(pair<Produto, int> prod : produtosComprados)
		pesoRecomendacao.push_back(make_pair(prod.first, 0));

	int posicao = 0;
	for(pair<Produto, int> prod : produtosComprados) {
		pesoRecomendacao[posicao].second += 5 * prod.second;
		for(string categoriaProduto : prod.first.get_categorias())
			for(pair<string, int> cat : categoriasCompradas) 
				if(categoriaProduto == cat.first)
					pesoRecomendacao[posicao].second += 3 * cat.second;

		posicao++;
	}

	sort(pesoRecomendacao.begin(), pesoRecomendacao.end(), sortRecomendacao);

	// Lista os produtos recomendados(máximo 10) para aquele cliente
	cout << "\tProdutos Recomendados:\n";
	recomendados.recomendacao(pesoRecomendacao);
}
