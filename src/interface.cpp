#include "interface.hpp"

#include <iostream>
#include <fstream>

Interface::Interface(size_t largura) {
	set_largura(largura);
}

Interface::~Interface(){}

size_t Interface::get_largura() {
	return largura;
}

void Interface::set_largura(size_t largura) {
	this->largura = largura;
}

void Interface::imprime_topo() {
    for(size_t i = 0; i <= get_largura(); i++) {
    	if(i == 0)
    		cout << "  \u2554";
    	else if(i == get_largura())
    		cout << "\u2557\n";
    	else
        	cout << "\u2550";
    }
}

void Interface::imprime_base() {
    for(size_t i = 0; i <= get_largura(); i++) {
    	if(i == 0)
    		cout << "  \u255A";
    	else if(i == get_largura())
    		cout << "\u255D\n";
    	else
        	cout << "\u2550";
    }
}

void Interface::imprime_espaco(size_t tamanho) {
	for(size_t i = 1; i <= tamanho; i++) {
		cout << " ";
	}
}

void Interface::imprime_bordas() {
    cout << "  \u2551";
    imprime_espaco(get_largura()-1);
    cout << "\u2551\n";
}


void Interface::imprime_bordas(string frase) {
	size_t tamanho;

	if(frase.length()%2 & 1) {
		tamanho = frase.length()/2;
		cout << "  \u2551";
		imprime_espaco(get_largura()/2 - tamanho);
		cout << frase;
		imprime_espaco(get_largura()/2 - tamanho - 1);
		cout << "\u2551\n";
	}
	else {
		tamanho = frase.length()/2;
		cout << "  \u2551";
		imprime_espaco(get_largura()/2 - tamanho);
		cout << frase;
		imprime_espaco(get_largura()/2 - tamanho);
		cout << "\u2551\n";
	}
}
