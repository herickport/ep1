#include "produto.hpp"
#include "estoque.hpp"
#include "categoria.hpp"
#include "interfaceprodutos.hpp"
#include "entrada.hpp"

#include <fstream>
#include <iostream>
#include <unistd.h>
#include <algorithm>

Produto::Produto() {}

Produto::Produto(int ID, string nome, float preco, int quantidade, vector<string> categorias) {
	set_ID(ID);
	set_nome(nome);
	set_preco(preco);
	set_quantidade(quantidade);
	set_categorias(categorias);
}

Produto::~Produto() {}

int Produto::get_ID() {
	return ID;
}

void Produto::set_ID(int ID) {
	this->ID = ID;
}

string Produto::get_nome() {
	return nome;
}

void Produto::set_nome(string nome) {
	this->nome = nome;
}

float Produto::get_preco() {
	return preco;
}

void Produto::set_preco(float preco) {
	this->preco = preco;
}

int Produto::get_quantidade() {
	return quantidade;
}

void Produto::set_quantidade(int quantidade) {
	this->quantidade = quantidade;
}

vector<string> Produto::get_categorias() {
	return categorias;
}

void Produto::set_categorias(vector<string> categorias) {
	this->categorias = categorias;
}

void Produto::cadastra_produto() {
	Produto * produto;
	Estoque * estoque;
	Categoria * categoria;
	InterfaceProdutos cadastroProduto(50);

	string nome;
	float preco;
	int quantidade;
	int ID_produto;
	vector<string> todasCategorias;
	vector<string> categorias_produto;
	char retorno;
	string nome_categoria;
	int numero_categoria;
	bool categoriaExistente = false;

	categoria = new Categoria();
	estoque = new Estoque();

	// Mini-interface para o cadastro de produtos
	cout << "\n  \u2554\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550";
	cout << "\u2550\u2550\u2550\u2550\u2550\u2550\u2550 Cadastro de Produto \u2550\u2550\u2550\u2550\u2550\u2550\u2550";
	cout << "\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\n  \u2551\n";
    cout << "  \u2551  Digite o nome do produto: ";
    getline(cin, nome);

    /*
		Transforma o nome do produto digitado para maiúsculo
		para que seja mais fácil verificar a duplicidade de
		produtos e manter um padrão nos nomes.
    */
    auto to_upper = [](char c) { return toupper(c); };
  	transform(nome.begin(), nome.end(), nome.begin(), to_upper);

    if (produto_cadastrado(nome)) {
    	cout << "  \u2551\n  \u2551  O produto já está cadastrado, ";
    	cout << "pressione enter para retornar ao Modo Estoque...";
        setbuf(stdin, NULL);
        scanf("%c", &retorno);
    	return;
    }

    cout << "  \u2551  Digite o preço do produto: R$ ";
    do {
    	preco = get_input<float>();

    	if(preco < 0)
    		cout << "  \u2551  Preço inválido, digite novamente: ";

    } while(preco < 0);

    cout << "  \u2551  Quantidade do produto em estoque: ";
    do {
    	quantidade = get_input<int>();

    	if(quantidade < 0)
    		cout << "  \u2551  Quantidade inválida, digite novamente: ";

    } while(quantidade < 0);
 
    cout << "  \u2551\n  \u2551  Escolha a(s) categoria(s) do produto:\n";
    todasCategorias = categoria->busca_categorias();
    setbuf(stdin, NULL);

    // Ordena as categorias por ordem lexicográfica
    sort(todasCategorias.begin(), todasCategorias.end());	

    int cont = 1;
    int aux = 0;
    if(todasCategorias.size() == 0) {
    	cout << "  \u2551\n  \u2551  Ainda não há categorias cadastradas...\n";
    	cout << "  \u2551\n  \u2551  Digite o nome da categoria para adicioná-la ao produto ";
	    cout << "ou \"concluir\" para encerrar o cadastro de categorias:\n  \u2551\n";

    	while(true) {
    		cout << "  \u2551  Categoria: ";
    		getline(cin, nome_categoria);
    		if(nome_categoria == "concluir")
    			break;

	    	/*
			Transforma o nome da categoria digitada para maiúsculo
			para que seja mais fácil verificar a duplicidade de
			categorias e manter um padrão nos nomes.
    		*/
    		transform(nome_categoria.begin(), nome_categoria.end(), nome_categoria.begin(), to_upper);
    		categoria->adiciona_categoria(nome_categoria);
    		categorias_produto.push_back(nome_categoria);
    	}
    } else {
    	// Lista todas as categorias cadastradas para que seja feita as escolhas
    	cout << "  \u2551\n  \u2551";
	    for(string cat : todasCategorias) {
	    	if(aux == 0) {
	    		printf("  (%d) %-15s", cont, cat.c_str());
	    		aux++;
	    	} else if(aux == 1) {
	    		printf("  (%d) %-15s", cont, cat.c_str());
	    		aux++;
	    	} else if(aux == 2) {
	    		printf("  (%d) %-15s\n  \u2551", cont, cat.c_str());
	    		aux = 0;
	    	}
	    	cont++;
	    }
	    cout << "  (0) Nova categoria\n";

	    cout << "  \u2551\n  \u2551  Digite o número da categoria para adicioná-la ao produto ";
	    cout << "ou \"concluir\" para encerrar a adição de categorias...\n  \u2551\n";
	    
	    do {
	    	cout << "  \u2551  Categoria: ";
	    	cin >> nome_categoria;

	    	if(nome_categoria == "concluir")
	    		break;

	    	numero_categoria = stoi(nome_categoria);
	    	if(numero_categoria > (int) todasCategorias.size() or numero_categoria < 0)
	    		cout << "  \u2551  Categoria inválida, digite novamente...\n  \u2551\n";
	    	else if(numero_categoria != 0)
	    		categorias_produto.push_back(todasCategorias[numero_categoria-1]);
	    } while(numero_categoria != 0);

	    if(numero_categoria == 0) {
	    	cout << "  \u2551\n  \u2551  Digite o nome da categoria para adicioná-la ao produto ";
	    	cout << "ou \"concluir\" para encerrar o cadastro de categorias:\n  \u2551\n";
	    	setbuf(stdin, NULL);
    		while(true) {
	    		cout << "  \u2551  Categoria: ";
	    		getline(cin, nome_categoria);
	    		if(nome_categoria == "concluir")
	    			break;

	    		transform(nome_categoria.begin(), nome_categoria.end(), nome_categoria.begin(), to_upper);
	    		for(string cat : todasCategorias)
	    			if(nome_categoria == cat)
	    				categoriaExistente = true;

	    		if(!categoriaExistente)
	    			categoria->adiciona_categoria(nome_categoria);
	    		else
	    			cout << "  \u2551  Esta categoria já existe e foi adicionada ao produto.\n";
	    		categorias_produto.push_back(nome_categoria);
    		}
	    }
	   	
    }

    // Busca a quantidade de produtds cadastrados para definir o ID do produto
    ID_produto = quantidade_produtos() + 1;

    produto = new Produto(ID_produto, nome, preco, quantidade, categorias_produto);

    estoque->adiciona_estoque(produto);
    categoria->adiciona_produto(produto);
    novo_produto(produto);
    arquiva_produto(produto);

    system("clear");
    cout << "\n  Produto cadastrado com sucesso, retornando ao Modo Estoque\n";
    sleep(2);
}

void Produto::novo_produto(Produto * produto) {
	ofstream produtos;

	produtos.open("./doc/geral/produtos.txt", ios::app);

	if(produtos.is_open())
		produtos << produto->get_nome() << endl;
		
	produtos.close(); 
}

void Produto::arquiva_produto(Produto * produto) {
	ofstream arquivo_produto;

	arquivo_produto.open("./doc/produtos/" + produto->get_nome() + ".txt", ios::app);

	if(arquivo_produto.is_open()) {
		arquivo_produto << produto->get_ID() << endl;
		arquivo_produto << produto->get_preco() << endl;
		for(string cat : produto->get_categorias())
			arquivo_produto << cat << endl;
	}

	arquivo_produto.close();
}

void Produto::imprime_produto() {
	vector<Produto> produtos;
	Estoque estoque;
	InterfaceProdutos tabela(65);

	produtos = estoque.recupera_estoque();

	tabela.tabela_estoque("Produtos em Estoque:", produtos);
}

bool Produto::produto_cadastrado(string nome_produto) {
	ifstream produtos_cadastrados;
	string nome;

	produtos_cadastrados.open("./doc/geral/produtos.txt");

	if(produtos_cadastrados.is_open()) {
		while(getline(produtos_cadastrados, nome)) {
			if(nome == nome_produto) {
				produtos_cadastrados.close();
				return true;
			}
		}
		produtos_cadastrados.close();
	}

	return false;
}

int Produto::quantidade_produtos() {
	ifstream todosProdutos;
	string produto;
	int qtd_produtos = 0;

	todosProdutos.open("./doc/geral/produtos.txt");

	if(todosProdutos.is_open())
		while(getline(todosProdutos, produto))
			qtd_produtos++;

	return qtd_produtos;
}

void Produto::atualiza_produto(vector<Produto> produtos) {
	Estoque * estoque;
	estoque = new Estoque;

	int id_produto;
	int quantidade;

	cout << "     Digite o ID do produto que deseja alterar a quantidade: ";
	do {
	    id_produto = get_input<int>();

	    if(id_produto > (int) produtos.size() or id_produto <= 0)
	    	cout << "     ID inválido, digite novamente: ";

	} while(id_produto > (int) produtos.size() or id_produto <= 0);

	cout << "     Digite a nova quantidade do produto em estoque: ";
	do {
	    quantidade = get_input<int>();

	    if(quantidade < 0)
	    	cout << "     Quantidade inválida, digite novamente: ";

	} while(quantidade < 0);

    estoque->atualiza_estoque(produtos[id_produto-1], quantidade);
}
