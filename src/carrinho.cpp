#include "carrinho.hpp"
#include "estoque.hpp"
#include "produto.hpp"
#include "interfaceprodutos.hpp"
#include "entrada.hpp"

#include <iostream>
#include <unistd.h>
#include <fstream>

using namespace std;

Carrinho::Carrinho() {}

Carrinho::~Carrinho() {}

void Carrinho::executar_compras(Cliente * cliente) {
	Estoque * estoque;
	InterfaceProdutos tabela(65);
	vector<Produto> produtos;
	vector<pair<Produto, int>> produtosComprados;

	float valorTotal = 0.0;
	float desconto = 0.0;
	float valorFinal = 0.0;

	int ID_produto;
	int quantidade;
	int concluir_compra;

	bool primeira_compra = false;

	estoque = new Estoque();
	produtos = estoque->recupera_estoque();

	cout << "  Digite o ID do produto desejado ou 0 para finalizar a compra: \n";
	while(true) {

		cout << "\n  ID: ";
		do {
			ID_produto = get_input<int>();
	
			if(ID_produto > (int) produtos.size() or ID_produto < 0)
				cout << "  ID inválido, digite novamente: ";

		} while(ID_produto > (int) produtos.size() or ID_produto < 0);

		if(ID_produto == 0)
				break;

		cout << "  Quantidade: ";
		do {
		    quantidade = get_input<int>();

		    if(quantidade < 0)
		    	cout << "  Quantidade inválida, digite novamente: ";

		} while(quantidade < 0);

		/*
			Para evitar que a compra fosse cancelada por causa da falta de um produto
			em estoque, eu achei mais viável e produtivo já verificar a quantidade em 
			estoque antes e avisar à funcionária a falta do produto em estoque. 
		*/
		while(quantidade > produtos[ID_produto-1].get_quantidade() or quantidade < 0) {
			cout << "  Quantidade não disponível, restam apenas ";
			cout << produtos[ID_produto-1].get_quantidade() << " deste produto em estoque...\n";
			cout << "  Nova Quantidade: ";
			quantidade = get_input<int>();
		}

		/* 
			Diminui temporariamente a quantidade de produtos em estoque
			para que seja possível verificar a quantidade existente
			caso ele seja adicionado novamente ao carrinho de compras.
			
		*/
		produtosComprados.push_back(make_pair(produtos[ID_produto-1], quantidade));

		produtos[ID_produto-1].set_quantidade(produtos[ID_produto-1].get_quantidade()-quantidade);
	}

	cout << "\n  Compra finalizada, carregando carrinho...\n";	
	sleep(1);


	valorTotal = valor_compra(produtosComprados);

	if(cliente->is_socio()) {
		desconto = valorTotal * 0.15;

		// Deixa o valor do desconto com apenas 2 casas decimais
		int transforma_desconto = desconto * 100;
		desconto = transforma_desconto / 100 + 0.01 * (transforma_desconto % 100);
	}

	if(cliente->is_socio() and cliente->get_total_compras() == 0) {
		primeira_compra = true;
		valorFinal = valorTotal - desconto + 5.00;
	} else {
		valorFinal = valorTotal - desconto;
	}

	system("clear");
	if(valorFinal != 0) {
		cout << "\tCarrinho de Compras:\n";
		tabela.mostra_carrinho(produtosComprados, valorTotal, desconto, valorFinal, primeira_compra);
	} else {
		cout << "\n  Nenhum produto foi comprado, retornando ao Modo Venda...\n";
		sleep(2);
		return;
	}

	cout << "\n  Concluir compra? (1) Sim / (0) Não - ";
	do {
		concluir_compra = get_input<int>();

		if(concluir_compra != 0 and concluir_compra != 1)
			cout << "\n  Opção inválida, escolha novamente: ";

	} while(concluir_compra != 0 and concluir_compra != 1);

	if(concluir_compra == 1) {
		cliente->set_total_compras(valorFinal + cliente->get_total_compras());

		// Quando ultrapassa 100 reais em compras o cliente vira sócio
		if(!cliente->is_socio() and cliente->get_total_compras() >= 100.0)
			cliente->set_socio(true);

		cliente->arquiva_cliente(cliente);

		arquiva_compra(produtosComprados, cliente);

		for(pair<Produto, int> prod : produtosComprados) 
			estoque->atualiza_estoque(prod.first, prod.first.get_quantidade()-prod.second);
		
	}

	system("clear");
	cout << "\n  Compra finalizada, retornando ao Modo Venda...\n";
	sleep(2);
}

void Carrinho::lista_produtos() {
	Estoque * estoque;
	InterfaceProdutos tabela(65);
	vector<Produto> produtos;

	estoque = new Estoque();

	produtos = estoque->recupera_estoque();

	cout << "\tProdutos Disponíveis:\n";
	tabela.produtos_venda(produtos);
}

float Carrinho::valor_compra(vector<pair<Produto, int>> produtos_comprados) {
	float valorTotal = 0.0;

	for(pair<Produto, int> produto : produtos_comprados)
		valorTotal += produto.first.get_preco() * produto.second;

	return valorTotal;
}

void Carrinho::arquiva_compra(vector<pair<Produto, int>> produtos_comprados, Cliente * cliente) {
	ofstream compra_cliente;

	compra_cliente.open("./doc/vendas/" + cliente->get_cpf() + ".txt", ios::app);

	if(compra_cliente.is_open()) {
		for(pair<Produto, int> produto : produtos_comprados) {
			compra_cliente << produto.second << endl;
			compra_cliente << produto.first.get_nome() << endl;
		}
	}
}
