#include "interfacemenu.hpp"

#include <fstream>
#include <iostream>

InterfaceMenu::InterfaceMenu() {
	set_largura(75);
}

InterfaceMenu::InterfaceMenu(size_t largura) {
	set_largura(largura);
}

InterfaceMenu::~InterfaceMenu() {}

void InterfaceMenu::imprime_topo(string titulo) {
	size_t tamanho;

	if(titulo.length()%2 & 1) {
		tamanho = titulo.length()/2 + 1;
		cout << "\n  \u2554";
		for(size_t i = 1; i <= get_largura()/2 - tamanho; i++) 
        	cout << "\u2550";
		cout << " " << titulo << " ";
		for(size_t i = 1; i <= get_largura()/2 - tamanho - 1; i++)
        	cout << "\u2550";
		cout << "\u2557\n";
	}
	else {
		tamanho = titulo.length()/2 + 1;
		cout << "\n  \u2554";
		for(size_t i = 1; i <= get_largura()/2 - tamanho; i++) 
        	cout << "\u2550";
		cout << " " << titulo << " ";
		for(size_t i = 1; i <= get_largura()/2 - tamanho; i++)
        	cout << "\u2550";
		cout << "\u2557\n";
	}
}

void InterfaceMenu::gera_menu(string titulo, string comando, vector<string> opcoes) {
    ifstream entrada;
	string texto;

	imprime_topo(titulo);
	imprime_bordas();

    entrada.open("./doc/logomarca/logo.asciiart");
    if(entrada.is_open()) {
        while(getline(entrada, texto)) {
            imprime_bordas(texto);
        }
        entrada.close();
    }

    imprime_bordas();
    imprime_bordas();
    imprime_bordas(comando);
    imprime_bordas();
    imprime_bordas();
    
    for(string opcao : opcoes) {
       imprime_bordas(opcao);
    }

    imprime_bordas();
    imprime_bordas();
    imprime_base();
}
