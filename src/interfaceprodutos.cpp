#include "interfaceprodutos.hpp"

#include <iostream>
#include <sstream>

InterfaceProdutos::InterfaceProdutos() {
	set_largura(75);
}

InterfaceProdutos::InterfaceProdutos(size_t largura) {
	set_largura(largura);
}

InterfaceProdutos::~InterfaceProdutos() {}

void InterfaceProdutos::imprime_meio() {
    for(size_t i = 0; i <= get_largura(); i++) {
    	if(i == 0)
    		cout << "  \u2560";
    	else if(i == get_largura())
    		cout << "\u2563\n";
    	else
        	cout << "\u2550";
    }
}

void InterfaceProdutos::imprime_produto(string frase) {
	size_t tamanho;

	tamanho = frase.length();
	cout << "  \u2551  ";
	cout << frase;
	imprime_espaco(get_largura() - tamanho - 3);
	cout << "\u2551\n";
}

string InterfaceProdutos::float_to_string(float valor) {
	ostringstream strs;
	strs << valor;
	string str = strs.str();

	return str;
}

void InterfaceProdutos::tabela_estoque(string titulo, vector<Produto> produtos) {

	cout << "	" << titulo << endl;
	imprime_topo();
	for(Produto prod : produtos) {
		//imprime_bordas();

		imprime_produto("Nome: " + prod.get_nome());

		imprime_produto("Preco: R$" + float_to_string(prod.get_preco()));
		imprime_produto("Quantidade: " + to_string(prod.get_quantidade()));

		
		string aux = "";
		for(string cat : prod.get_categorias()) {
			if(aux == "")
				aux = cat;
			else
				aux = aux + ", " + cat;
		}
		

		imprime_produto("Categorias: " + aux);
		//imprime_bordas();
		imprime_meio();
	}

	imprime_bordas("Fim do Estoque");
	imprime_base();
}


void InterfaceProdutos::produtos_venda(vector<Produto> produtos) {

	imprime_topo();
	for(Produto prod : produtos) {
		imprime_produto("ID: " + to_string(prod.get_ID()));
		imprime_produto("Nome: " + prod.get_nome());
		imprime_produto("Preco: R$" + float_to_string(prod.get_preco()));
		imprime_meio();
	}

	imprime_bordas("Fim da Relacao de Produtos");
	imprime_base();
}

void InterfaceProdutos::mostra_carrinho(vector<pair<Produto, int>> produtos_comprados, float valorTotal,
	float desconto,	float valorFinal, bool primeira_compra_socio) {
	imprime_topo();

	for(pair<Produto, int> produto : produtos_comprados) {
		imprime_produto("Produto: " + produto.first.get_nome());

		string preco_unitario = float_to_string(produto.first.get_preco());
		string preco_total = float_to_string(produto.first.get_preco() * produto.second);

		imprime_produto("Preco Total: " + to_string(produto.second) + " x " + preco_unitario + " = R$ " + preco_total);
		imprime_meio();
	}

	imprime_bordas("Resultado da compra:");
	imprime_meio();
	imprime_produto("Valor Total: R$ " + float_to_string(valorTotal));
	imprime_produto("Desconto socio: R$ " + float_to_string(desconto));

	if(primeira_compra_socio)
		imprime_produto("Taxa Socio: R$ 5.00");

	imprime_produto("Valor Final: R$ " + float_to_string(valorFinal));
	imprime_base();
}

void InterfaceProdutos::recomendacao(vector<pair<Produto, int>> pesoRecomendacao) {
	imprime_topo();

	int cont = 0;
	for(pair<Produto, int> peso : pesoRecomendacao) {
		imprime_produto(to_string(cont+1) + " - " + peso.first.get_nome());
		imprime_meio();
		cont++;
		if(cont == 10)
			break;
	}

	imprime_bordas("Fim dos produtos recomendados");
	imprime_base();
}

void InterfaceProdutos::produtos_estoque(vector<Produto> produtos) {
	imprime_topo();

	for(Produto prod : produtos) {
		imprime_produto("Id: " + to_string(prod.get_ID()));
		imprime_produto("Nome: " + prod.get_nome());
		imprime_produto("Quantidade: " + to_string(prod.get_quantidade()));
		imprime_meio();
	}

	imprime_bordas("Quantidade de Produtos em estoque");
	imprime_base();
}
