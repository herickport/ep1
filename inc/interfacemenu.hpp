#ifndef INTERFACE_MENU_HPP
#define INTERFACE_MENU_HPP

#include "interface.hpp"

class InterfaceMenu : public Interface {
private:
	/* 
		Faz uma sobrescrita do método da classe pai imprimindo,
		além do topo do menu, um título centralizado.
	*/
	void imprime_topo(string titulo);
public:
	// Construtores e destrutor
	InterfaceMenu();
	InterfaceMenu(size_t largura);
	~InterfaceMenu();

	/*	
		Método que gera um menu, apresentando as opções de escolha,
		utilizando os demais métodos da classe filha e da classe pai.
	*/
	void gera_menu(string titulo, string comando, vector<string> opcoes);
};

#endif