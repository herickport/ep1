#ifndef CATEGORIA_HPP
#define CATEGORIA_HPP

#include "produto.hpp"

#include <vector>
#include <string>

using namespace std;

class Categoria {
public:
	Categoria();
	~Categoria();

	// Adiciona uma nova categoria ao arquivo que guarda todas as categorias
	void adiciona_categoria(string nome_categoria);

	// Retorna um vector de string contendo todas as categorias cadastradas no sistema
	vector<string> busca_categorias();

	/*
		Adiciona um novo produto ao arquivo individual da categoria,
		que guarda todos os produtos daquela categoria.
	*/
	void adiciona_produto(Produto * produto);
};

#endif