#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP

#include "produto.hpp"

#include <vector>

class Estoque {
public:
	Estoque();
	~Estoque();

	// Adiciona um novo produto ao estoque, colocando sua quantidade apenas
	void adiciona_estoque(Produto * produto);

	// Retorna todos os produtos cadastrados na loja
	vector<Produto> recupera_estoque();

	// Atualiza a quantidade de produtos em estoque
	void atualiza_estoque(Produto produto, int novaQuantidade);

	// Retorna verdadeiro se há produtos cadastrados e falso caso contrário
	bool produto_em_estoque();
};

#endif