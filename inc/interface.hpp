#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include <string>
#include <vector>

#include "produto.hpp"

using namespace std;

// Todo o modelo da interface foi feito utilizando unicode 

class Interface {
protected:
	size_t largura;
	
	// Imprime o topo da tabela/menu
	virtual void imprime_topo();

	// Imprime a base da tabela/menu
	void imprime_base();

	// Imprime espaços em branco de acordo com o parâmetro passado
	void imprime_espaco(size_t tamanho);

	// Imprime a lateral da tabela/menu, sem nada no meio (apenas espaços)
	void imprime_bordas();

	// Imprime a lateral da tabela/menu, com uma frase centralizada
	void imprime_bordas(string frase);
public:
	// Construtores e destrutor
	Interface() : largura(75) {};
	Interface(size_t largura);
	~Interface();

	// Métodos acessores
	void set_largura(size_t largura);
	size_t get_largura();
};

#endif