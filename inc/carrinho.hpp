#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include "produto.hpp"
#include "cliente.hpp"

#include <vector>
#include <utility>

using namespace std;

class Carrinho {
private:
	// Calcula o valor total da compra
	float valor_compra(vector<pair<Produto, int>> produtosComprados);

	/*
		Salva a compra em um arquivo que guarda todas as compras realizadas
		por aquele cliente em específico, para que seja possível fazer uma
		recomendação de produtos para ele posteriormente
	*/
	void arquiva_compra(vector<pair<Produto, int>> produtos_comprados, Cliente * cliente);
public:
	Carrinho();
	~Carrinho();

	/*
		Executa todo o processo da compra, escolhendo os produtos pelo ID
		e adicionando a um std::vector<std::pair<Produto, int>> que guarda
		todos os produtos comprados e a quantidade comprada de cada um.
	*/
	void executar_compras(Cliente * cliente);

	/*
		Por meio da classe de interface dos produtos, lista todos os produtos
		disponíveis para a venda, com seus respectivos ID, nome e preço.
	*/
	void lista_produtos();
};

#endif