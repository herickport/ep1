#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include "produto.hpp"

#include <string>
#include <vector>
#include <utility>

using namespace std;

class Cliente {
private:
	// Atributos do cliente
	string nome;
	string cpf;
	float total_compras;
	bool socio;

	// Métodos utilizados apenas na classe cliente, e por isso privados:

	// Adiciona o cliente cadastrado ao arquivo que guarda todos os clientes da loja
	void novo_cliente(Cliente * cliente);

	/*
		Recebe o ponteiro de um cliente a ser analisado e retorna um vector contendo
		todos os produtos comprados por aquele cliente acompanhado da quantidade
		comprada de cada um para serem usadas no cálculo da recomendação de cada produto. 
	*/
	vector<pair<Produto, int>> produtos_comprados(Cliente * cliente);

	/*
		Recebe todos os produtos e a quantidade comprada por um determinado cliente e retorna
		um vector contendo todas as categorias compradas por aquele cliente acompanhado da
		quantidade comprada de cada uma para serem usadas no cálculo da recomendação de cada produto. 
	*/
	vector<pair<string, int>> categorias_compradas(vector<pair<Produto, int>> produtosComprados);
public:
	// Construtores e destrutor
	Cliente();
	Cliente(string nome, string cpf, bool socio);
	Cliente(string nome, string cpf, bool socio, float total_compras);
	~Cliente();

	// Métodos acessores
	string get_nome();
	void set_nome(string nome);
	string get_cpf();
	void set_cpf(string cpf);
	float get_total_compras();
	void set_total_compras(float total_compras);
	bool is_socio();
	void set_socio(bool socio);


	// Cadastra um cliente e retorna um ponteiro para ele
	Cliente * cadastra_cliente();

	// Retorna verdadeiro se o cliente já está cadastrado e falso caso contrário
	bool cliente_cadastrado(string cpf_cliente);

	// Acessa o arquivo do cliente e recupera seus dados 
	Cliente * recupera_cliente(string cpf_cliente);

	/*
		Confere se o cliente já está cadastrado na loja, caso o cliente já esteja cadastrado
		é chamado o método Cliente::recupera_cliente e assim são obtidos seus dados, caso
		contrário é chamada o método Cliente::cadastra_cliente e o cliente é cadastrado na loja.
	*/
	Cliente * valida_cliente();

	// Cria um arquivo para o cliente, contendo suas informações
	void arquiva_cliente(Cliente * cliente);

	/*
		Por meio dos métodos Cliente::produtos_comprados e Cliente::categorias_compradas,
		e utilizando peso 5 para cada vez que o cliente comprou daquele produto e peso 3
		para cada vez que ele comprou das categorias daquele produto, é calculado o nível de
		recomendação de cada produto para aquele cliente em específico e apresentado na tela.
	*/
	void recomenda_produtos(Cliente * cliente);
};

#endif