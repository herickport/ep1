#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <string>
#include <vector>

using namespace std;

class Produto {
private:
	// Atributos do produto
	int ID;
	string nome;
	float preco;
	int quantidade;
	vector<string> categorias;

	// Métodos utilizados apenas na classe Produto, e por isso privados:

	// Adiciona o produto cadastrado ao arquivo que guarda todos os produtos cadastrados da loja
	void novo_produto(Produto * produto);

	// Cria um arquivo para o produto, contendo todas as suas informações
	void arquiva_produto(Produto * produto);

public:
	// Construtores e destrutor
	Produto();
	Produto(int ID, string nome, float preco, int quantidade, vector<string> categorias);
	~Produto();

	// Métodos Acessores
	int get_ID();
	void set_ID(int ID);
	string get_nome();
	void set_nome(string nome);
	float get_preco();
	void set_preco(float preco);
	int get_quantidade();
	void set_quantidade(int quantidade);
	vector<string> get_categorias();
	void set_categorias(vector<string> categorias);

	// Cadastra um novo produto para ser vendido na loja
	void cadastra_produto();

	// Recupera todos os produtos em estoque e utiliza a classe InterfaceProdutos para imprimir na tela
	void imprime_produto();

	// Retorna verdadeiro se o produto já está cadastrado e falso caso contrário
	bool produto_cadastrado(string nome_produto);

	// Retorna a quantidade de produtos cadastrados
	int quantidade_produtos();

	// Recebe o Id e a nova quantidade de produtos para que seja possível atualizar o estoque
	void atualiza_produto(vector<Produto> produtos);
};

#endif