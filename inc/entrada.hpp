#include <iostream>

using namespace std;

// Função criada para validar a entrada do usuário

template<typename Entrada>
Entrada get_input() {
	Entrada valor;
    cin >> valor;
    if(cin.fail()) {
        cin.clear();
        cin.ignore(32767,'\n');
            return -1;
    }
    else {
        cin.ignore(32767,'\n');
        return valor;
    }
}