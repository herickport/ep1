#ifndef INTERFACE_PRODUTOS_HPP
#define INTERFACE_PRODUTOS_HPP

#include "interface.hpp"

#include <utility>

class InterfaceProdutos : public Interface {
private:
	// Imprime uma linha em unicode de uma borda à outra 
	void imprime_meio();

	// Imprime uma frase alinhada à esquerda, com espaços em branco até a outra borda
	void imprime_produto(string frase);

	// Transforma de float para string
	string float_to_string(float valor);
public:
	// Construtores e destrutor
	InterfaceProdutos();
	InterfaceProdutos(size_t largura);
	~InterfaceProdutos();

	/*
		Apresenta o nome, preço, quantidade e as categorias do produto para
		que seja feita a visuzalização de todos os produtos existentes no estoque.
	*/
	void tabela_estoque(string titulo, vector<Produto> produtos);

	/*
		Apresenta o ID, nome e preço do produto antes da venda para que
		o cliente possa escolher o ID do produto desejado na compra.
	*/
	void produtos_venda(vector<Produto> produtos);

	/*
		Apresenta o carrinho de compras, com todos os produto comprados,
		o valor total, desconto e o valor final da compra realizada.
	*/
	void mostra_carrinho(vector<pair<Produto, int>> produtos_comprados, float valorTotal, float desconto,
		float valorFinal, bool primeira_compra_socio);

	/*
		Apresenta uma tabela com os produtos recomendados para aquele cleinte
		de acordo com o peso da recomendação de cada produto.
	*/
	void recomendacao(vector<pair<Produto, int>> pesoRecomendacao);

	// Apresenta os produtos em estoque, para que seja feita a atualização da quantidade
	void produtos_estoque(vector<Produto> produtos);
};

#endif